'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Instance extends Model {

  static get computed(){
    return ['url']
  }

  getUrl({ api_id, api_token }){
    return `https://api.z-api.io/instances/${api_id}/token/${api_token}`
  }

  city(){
    return this.belongsTo('App/Models/City')
  }
}

module.exports = Instance
