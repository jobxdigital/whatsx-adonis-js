'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class City extends Model {
  instances(){
    return this.hasMany('App/Models/Instance')
  }
}

module.exports = City
