'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class List extends Model {
  instance(){
    return this.belongsTo('App/Models/Instance')
  }
}

module.exports = List
