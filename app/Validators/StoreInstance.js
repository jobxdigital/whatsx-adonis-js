'use strict'

class StoreInstance {
  async fails(errorMessages){
    this.ctx.session
      .withErrors(errorMessages).flashAll()

    return this.ctx.response.redirect('back')
  }

  get sanitizationRules () {
    return {
      city_id: 'to_int'
    }
  }

  get validateAll () {
    return true
  }

  get rules () {
    return {
      name: 'required',
      api_id: 'required',
      api_token: 'required',
      city_id: 'required'
    }
  }

  get messages() {
    return {
      'name.required': 'Por favor, digite um nome pra instância. Dê preferência a um nome que nunca foi usado pois pode causar confusão.',
      'api_id.required': 'Por favor, insira o id da instância.',
      'api_token.required': 'Por favor, insira o token da instância.',
      'city_id.required': 'É obrigatório relacionar a instância com uma cidade',
    }
  }
}

module.exports = StoreInstance
