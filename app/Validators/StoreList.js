'use strict'

class StoreList {
  get validateAll () {
    return true
  }

  get rules () {
    return {
      name: 'required',
      theme: 'required',
      city_id: 'required',
      list: 'file|file_ext:csv|file_size:5mb|file_types:csv,text/csv,application/csv,vnd.ms-excel,octet-stream'
    }
  }

  async fails (errorMessages) {
    this.ctx.session
      .withErrors(errorMessages).flashAll()

    return this.ctx.response.redirect('back')
  }

  get sanitizationRules () {
    return {
      city_id: 'to_int'
    }
  }

  get messages () {
    return {
      'name.required':
        'Por favor, digite um nome pra lista. Dê preferência a um nome que nunca foi usado pois pode causar confusão.',
      'theme.required': 'Por favor, insira o tema',
      'city_id.required':
        'É obrigatório que você relacione a lista com uma cidade',
      'list.file': 'É obrigatório a entrada de um csv',
    }
  }
}

module.exports = StoreList
