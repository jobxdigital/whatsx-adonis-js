'use strict'

class StoreCity {
  get validateAll () {
    return true
  }

  async fails(errorMessages){
    this.ctx.session
      .withErrors(errorMessages).flashAll()

    return this.ctx.response.redirect('back')
  }

  get rules () {
    return {
      name: 'required'
    }
  }

  get messages() {
    return {
      'name.required': 'Por favor, digite um nome pra cidade'
    }
  }
}

module.exports = StoreCity
