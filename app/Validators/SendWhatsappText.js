'use strict'

class SendWhatsappText {
  get validateAll(){
    return true
  }

  async fails(errorMessages){
    this.ctx.session
      .withErrors(errorMessages).flashAll()

    return this.ctx.response.redirect('back')
  }

  get rules () {
    return {
      list_id: 'required',
      message: 'required',
      delay: 'required'
    }
  }

  get messages(){
    return {
      'list_id.required': 'É preciso que você selecione uma lista',
      'message.required': 'Insira uma mensagem',
      'delay.required': 'É preciso configurar um delay entre cada mensagem'
    }
  }
}

module.exports = SendWhatsappText
