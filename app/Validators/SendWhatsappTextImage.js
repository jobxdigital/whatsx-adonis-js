'use strict'

class SendWhatsappTextImage {
  get validateAll(){
    return true
  }

  async fails(errorMessages){
    this.ctx.session
      .withErrors(errorMessages).flashAll()

    return this.ctx.response.redirect('back')
  }

  get rules () {
    return {
      list_id: 'required',
      image: 'file|file_ext:png,jpg,gif,jpeg|file_size:5mb|file_types:image',
      primeiro: 'required',
      delay: 'required',
      message: 'required',
    }
  }

  get messages(){
    return {
      'list_id.required': 'É preciso que você selecione uma lista',
      'image.file': 'Deve ser enviado uma imagem',
      'delay.required': 'É preciso configurar um delay entre cada mensagem',
      'primeiro.required': 'Deve ser escolhido quem vai ser mandando primeiro',
      'message.required': 'Insira uma mensagem',
    }
  }
}

module.exports = SendWhatsappTextImage
