'use strict'

class SendWhatsappVideo {
  get validateAll(){
    return true
  }

  async fails(errorMessages){
    this.ctx.session
      .withErrors(errorMessages).flashAll()

    return this.ctx.response.redirect('back')
  }

  get rules () {
    return {
      list_id: 'required',
      delay: 'required',
      video: 'required'
    }
  }

  get messages(){
    return {
      'list_id.required': 'É preciso que você selecione uma lista',
      'delay.required': 'É preciso configurar um delay entre cada mensagem',
      'video.required': 'Envie o vídeo'
    }
  }
}

module.exports = SendWhatsappVideo
