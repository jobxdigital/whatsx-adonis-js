'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Instance = use('App/Models/Instance')

class InstancesList {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, view, session }, next) {
    const { rows: instances } = await Instance
      .query()
      .where('city_id', session.get('currentCity'))
      .fetch()

    view.share({
      instances
    })

    await next()
  }
}

module.exports = InstancesList
