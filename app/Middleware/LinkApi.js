'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Instance = use('App/Models/Instance')

class LinkApi {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, session, view }, next) {
    const currentInstance = session.get('currentInstance')

    const instance = await Instance.find(+currentInstance)

    view.share({
      url: instance.toJSON().url,
    })

    await next()
  }
}

module.exports = LinkApi
