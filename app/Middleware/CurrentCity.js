'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @typedef {import('@adonisjs/framework/src/Session')} Session */

const City = use('App/Models/City')

class CurrentCity {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, session, view }, next) {
    if (session.get('currentCity') === null) {
      const { rows: cities } = await City.all()
      session.put('currentCity', String(cities[0].id))
    }

    view.share({
      currentCity: session.get('currentCity'),
    })
    // call next to advance the request
    await next()
  }
}

module.exports = CurrentCity
