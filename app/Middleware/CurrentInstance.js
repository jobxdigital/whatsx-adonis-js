'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Instance = use('App/Models/Instance')

class CurrentInstance {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, session, view }, next) {
    if (session.get('currentInstance') === null) {
      const { rows: instances } = await Instance.all({
        city_id: Number(session.get('currentCity')),
      })
      session.put('currentInstance', String(instances[0].id))
    }

    view.share({
      currentInstance: session.get('currentInstance'),
    })
    // call next to advance the request
    await next()
  }
}

module.exports = CurrentInstance
