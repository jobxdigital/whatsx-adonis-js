'use strict'

const List = use('App/Models/List')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class Lists {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, session, view }, next) {
    const { rows: lists } = await List.query()
      .where('city_id', session.get('currentCity'))
      .fetch()

    view.share({
      lists,
    })

    // call next to advance the request
    await next()
  }
}

module.exports = Lists
