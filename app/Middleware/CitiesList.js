'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const City = use('App/Models/City')

class CitiesList {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, view }, next) {
    const { rows: cities } = await City.all()

    view.share({
      cities
    })

    await next()
  }
}

module.exports = CitiesList
