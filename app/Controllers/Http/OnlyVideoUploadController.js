'use strict'

const Drive = use('Drive')

class OnlyVideoUploadController {
  async index({ view }) {
    return view.render('pages/upload-video')
  }

  async store({ request, response, session }) {
    await request.multipart
      .file('video', {}, async (file) => {
        try {
          const ContentType = file.headers['content-type']
          const ACL = 'public-read'
          const Key = `${(Math.random() * 100).toString(32)}-${file.clientName}`

          const url = await Drive.put(Key, file.stream, {
            ContentType,
            ACL,
          })

          return response.redirect(`/whatsapp/video?url_video=${url}`)
        } catch (err) {
          session.flash({ error: 'Problema ao enviar o vídeo' })
          return response.redirect('back')
        }
      })
      .process()
  }
}

module.exports = OnlyVideoUploadController
