'use strict'

const Database = use('Database')
const Instance = use('App/Models/Instance')

const path = require('path')
const { promises: fs } = require('fs')
const Helpers = use('Helpers')

const formatTelephone = require('../../../helpers/formatTelephone')
const sendWhatsappImage = require('../../../helpers/sendWhatsappImage')
const sendWhatsappText = require('../../../helpers/sendWhatsappText')
const readMultipleCsv = require('../../../helpers/readMultipleCsv')
const delayGen = require('../../../helpers/delayGen')

class SendWhatsappTextImageController {
  async index({ view }) {
    return view.render('pages/send-text-image')
  }

  async store({ request, response, session }) {
    try {
      const { list_id, message, delay, primeiro } = request.all()

      const lists = await Database.table('lists').whereIn(
        'id',
        Array.isArray(list_id) ? list_id : [list_id]
      )

      const currentInstance = session.get('currentInstance')
      const instance = await Instance.find(+currentInstance)

      const imageFile = request.file('image')

      await imageFile.move(Helpers.tmpPath('uploads'), {
        name: imageFile.clientName,
        overwrite: true,
      })

      const imagePath = path.join(
        Helpers.tmpPath('uploads'),
        imageFile.clientName
      )

      if (!imageFile.moved()) {
        session.flash({ error: 'Problema ao enviar imagem' })
        return response.redirect('back')
      }

      const imageBase64 = await fs.readFile(imagePath, 'base64')

      const urlInstance = instance.toJSON().url

      const dataBase64 = imageFile.type + '/' + imageFile.subtype

      const [min, max] = delay.split(',').map((interval) => Number(interval))

      let accumulator = max * 1000

      const gen = delayGen({ max, min, diff: 5 })

      const paths = lists.map((list) =>
        path.resolve(Helpers.tmpPath('uploads'), list.csv_path)
      )

      readMultipleCsv(paths, (data) => {
        const firstKey = Object.keys(data)[0]

        const request = {
          phone: formatTelephone(data[firstKey]),
          image: `data:${dataBase64};base64,` + imageBase64,
          urlInstance,
          message,
        }

        setTimeout(async () => {
          try {
            if (primeiro === 'imagem') {
              await sendWhatsappImage(request)
              await sendWhatsappText(request)
            } else {
              await sendWhatsappText(request)
              await sendWhatsappImage(request)
            }
          } catch (err) {
            console.log(err)
          }
        }, accumulator)

        accumulator += gen() * 1000
      })

      await fs.unlink(imagePath)

      session.flash({ success: 'Mensagens enfileiradas com sucesso!' })
      return response.redirect('back')
    } catch (err) {
      console.log(err)
      session.flash({ error: 'Problema ao enviar uma ou mais mensagens' })
      return response.redirect('back')
    }
  }
}

module.exports = SendWhatsappTextImageController
