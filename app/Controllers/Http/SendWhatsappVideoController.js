'use strict'

const Database = use('Database')
const Instance = use('App/Models/Instance')
const path = require('path')
const Helpers = use('Helpers')

const readMultipleCsv = require('../../../helpers/readMultipleCsv')
const formatTelephone = require('../../../helpers/formatTelephone')
const sendWhatsappVideo = require('../../../helpers/sendWhatsappVideo')
const delayGen = require('../../../helpers/delayGen')

class SendWhatsappVideoController {
  async index({ request, view }) {
    const { url_video } = request.get()

    return view.render('pages/send-video', { url_video })
  }

  async store({ request, response, view, session }) {
    try {
      const { list_id, delay, video } = request.all()

      const lists = await Database.table('lists').whereIn(
        'id',
        Array.isArray(list_id) ? list_id : [list_id]
      )

      const currentInstance = session.get('currentInstance')
      const instance = await Instance.find(+currentInstance)

      const urlInstance = instance.toJSON().url

      const [min, max] = delay.split(',').map((interval) => Number(interval))

      let accumulator = max * 1000

      const gen = delayGen({ max, min, diff: 5 })

      const paths = lists.map((list) =>
        path.resolve(Helpers.tmpPath('uploads'), list.csv_path)
      )

      readMultipleCsv(paths, (data) => {
        const firstKey = Object.keys(data)[0]

        const requestBody = {
          phone: formatTelephone(data[firstKey]),
          video: encodeURI(video),
          urlInstance,
        }

        setTimeout(async () => {
          try {
            await sendWhatsappVideo(requestBody)
          } catch (err) {
            console.log(err)
          }
        }, accumulator)

        accumulator += gen() * 1000
      })

      session.flash({ success: 'Mensagens enfileiradas com sucesso!' })
      return response.redirect('back')
    } catch (err) {
      console.log(err)
      session.flash({ error: 'Problema ao enviar uma ou mais mensagens' })
      return response.redirect('back')
    }
  }
}

module.exports = SendWhatsappVideoController
