'use strict'

const Helpers = use('Helpers')
const Database = use('Database')
const List = use('App/Models/List')
const path = require('path')
const csvtojson = require('csvtojson')
const fs = require('fs')
const { promisify } = require('util')
const { pipeline } = require('stream')
const { Transform } = require('stream')
const formatTelephone = require('../../../helpers/formatTelephone')
const pipelineAsync = promisify(pipeline)
const unlinkAsync = promisify(fs.unlink)

class ListController {
  async index({ view }) {
    return view.render('pages/register-list')
  }

  async store({ request, response, session }) {
    const trx = await Database.beginTransaction()

    try {
      const csvFile = request.file('list', {
        types: [
          'csv',
          'text/csv',
          'application/csv',
          'vnd.ms-excel',
          'octet-stream',
        ],
        size: '5mb',
      })

      const timestamp = new Date().getTime()
      const fileNameSplitted = csvFile.stream.filename.split('.')

      const extension = fileNameSplitted.pop()
      const realFilename = fileNameSplitted.join('')

      const slugFilename = realFilename
        .normalize('NFD')
        .replace(/[^a-zA-Zs]/g, '')
      const csvFilenameDirty = `${timestamp}_${slugFilename}_dirty.${extension}`
      const csvFilenameClean = `${timestamp}_${slugFilename}.${extension}`

      await csvFile.move(Helpers.tmpPath('uploads'), {
        name: csvFilenameDirty,
        overwrite: true,
      })

      if (!csvFile.moved()) {
        await trx.rollback()
        session.flash({ error: 'Erro ao enviar csv' })
        return response.redirect('back')
      }

      const csvDirty = path
        .resolve(Helpers.tmpPath('uploads'), csvFilenameDirty)
      const csvClean = path.resolve(Helpers.tmpPath('uploads'), csvFilenameClean)

      const numbers = new Set()

      await pipelineAsync(
        fs.createReadStream(csvDirty),
        csvtojson(),
        new Transform({
          transform: (chunk, encoding, cb) => {
            const telephone = JSON.parse(chunk)

            const column = Object.keys(telephone)[0]

            const tel = formatTelephone(telephone[column])

            if(!numbers.has(column) && !numbers.has(tel)){
              numbers.add(column)
              numbers.add(tel)
              return cb(null, `${column}\n${tel}\n`)
            }

            if(!numbers.has(tel)){
              numbers.add(tel)
              return cb(null, `${tel}\n`)
            }

            return cb(null, '')
          },
        }),
        fs.createWriteStream(csvClean)
      )

      const { name, theme, city_id } = request.all()

      await List.create(
        { name, theme, city_id, csv_path: csvFilenameClean },
        trx
      )

      await trx.commit()

      await unlinkAsync(csvDirty)

      session.flash({ success: 'Lista cadastrada com sucesso!' })
      return response.redirect('back')
    } catch (err) {
      console.log(err)
      await trx.rollback()

      session.flash({ error: 'Erro inesperado' })
      return response.redirect('back')
    }
  }
}

module.exports = ListController
