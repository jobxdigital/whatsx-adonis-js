'use strict'

class PlatformController {
  async index({ view }) {
    return view.render('pages/home-platform', {})
  }
}

module.exports = PlatformController
