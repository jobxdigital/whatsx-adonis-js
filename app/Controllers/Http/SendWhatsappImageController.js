'use strict'

const Helpers = use('Helpers')
const Database = use('Database')
const Instance = use('App/Models/Instance')
const { promises: fs } = require('fs')
const path = require('path')

const formatTelephone = require('../../../helpers/formatTelephone')
const sendWhatsappImage = require('../../../helpers/sendWhatsappImage')
const readMultipleCsv = require('../../../helpers/readMultipleCsv')
const delayGen = require('../../../helpers/delayGen')

class SendWhatsappImageController {
  async index({ view }) {
    return view.render('pages/send-image')
  }

  async store({ request, response, session }) {
    try {
      const { list_id, delay } = request.all()

      const lists = await Database.table('lists').whereIn(
        'id',
        Array.isArray(list_id) ? list_id : [list_id]
      )

      const currentInstance = session.get('currentInstance')
      const instance = await Instance.find(+currentInstance)

      const imageFile = request.file('image')

      await imageFile.move(Helpers.tmpPath('uploads'), {
        name: imageFile.clientName,
        overwrite: true,
      })

      const imagePath = path.join(
        Helpers.tmpPath('uploads'),
        imageFile.clientName
      )

      if (!imageFile.moved()) {
        session.flash({ error: 'Problema ao enviar imagem' })
        return response.redirect('back')
      }

      const imageBase64 = await fs.readFile(path.resolve(imagePath), 'base64')

      const urlInstance = instance.toJSON().url

      const dataBase64 = imageFile.type + '/' + imageFile.subtype

      const [min, max] = delay.split(',').map((interval) => Number(interval))

      let accumulator = max * 1000

      const gen = delayGen({ max, min, diff: 5 })

      const paths = lists.map((list) =>
        path.resolve(Helpers.tmpPath('uploads'), list.csv_path)
      )

      readMultipleCsv(paths, async (data) => {
        const firstKey = Object.keys(data)[0]

        const requestBody = {
          phone: formatTelephone(data[firstKey]),
          image: `data:${dataBase64};base64,` + imageBase64,
          urlInstance,
        }

        setTimeout(async () => {
          try {
            await sendWhatsappImage(requestBody)
          } catch (err) {
            console.log(err)
          }
        }, accumulator)

        accumulator += gen() * 1000
      })

      await fs.unlink(imagePath)

      session.flash({ success: 'Mensagens enfileiradas com sucesso!' })
      return response.redirect('back')
    } catch (err) {
      console.log(err)
      session.flash({ error: 'Problema ao enfileirar uma ou mais mensagens' })
      return response.redirect('back')
    }
  }
}

module.exports = SendWhatsappImageController
