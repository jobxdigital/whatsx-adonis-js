'use strict'

const City = use('App/Models/City')
const Instance = use('App/Models/Instance')

class SessionController {
  async changeCity({ request, response, session }) {
    const { city } = request.params

    if (!city) {
      return response.status(400).json({ error: 'Cidade não foi fornecida' })
    }
    const existsCityOrFail = await City.find(+city)

    if (!existsCityOrFail) {
      return response.status(404).json({ error: 'Cidade não foi encontrada' })
    }

    session.put('currentCity', city)

    const newCurrent = await Instance.findBy('city_id', city)

    session.put('currentInstance', String(newCurrent.id))

    return response.status(204).json()
  }

  async changeInstance({ request, response, session }) {
    const { instance } = request.params

    if (!instance) {
      return response.status(400).json({ error: 'Instancia não foi fornecida' })
    }

    const existsInstance = await Instance.query()
      .where('city_id', session.get('currentCity'))
      .where('id', +instance)
      .fetch()

    if (!existsInstance) {
      return response.status(404).json({
        error: 'Cidade não foi encontrada',
      })
    }

    session.put('currentInstance', instance)

    return response.status(204).json()
  }
}

module.exports = SessionController
