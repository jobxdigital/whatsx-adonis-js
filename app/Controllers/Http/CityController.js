'use strict'

const City = use('App/Models/City')

class CityController {
  async index({ view }) {
    return view.render('pages/register-city')
  }

  async store({ request, response, session }) {
    try {
      const { name } = request.all()

      await City.create({ name })

      session.flash({ success: 'Cidade cadastrada com sucesso!' })
      return response.redirect('back')
    } catch (err) {
      session.flash({ error: 'Erro inesperado' })
      return response.redirect('back')
    }
  }
}

module.exports = CityController
