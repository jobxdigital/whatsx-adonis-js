'use strict'

const Helpers = use('Helpers')
const Instance = use('App/Models/Instance')
const Database = use('Database')
const path = require('path')

const readMultipleCsv = require('../../../helpers/readMultipleCsv')
const formatTelephone = require('../../../helpers/formatTelephone')
const sendWhatsappText = require('../../../helpers/sendWhatsappText')
const delayGen = require('../../../helpers/delayGen')

class SendWhatsappTextController {
  async store({ request, response, session }) {
    try {
      const { list_id, message, delay } = request.all()

      const lists = await Database.table('lists').whereIn(
        'id',
        Array.isArray(list_id) ? list_id : [list_id]
      )

      const currentInstance = session.get('currentInstance')
      const instance = await Instance.find(+currentInstance)
      const urlInstance = instance.toJSON().url

      const [min, max] = delay.split(',').map((interval) => Number(interval))

      let accumulator = max * 1000

      const gen = delayGen({ max, min, diff: 5 })

      const paths = lists.map((list) =>
        path.resolve(Helpers.tmpPath('uploads'), list.csv_path)
      )

      readMultipleCsv(paths, (data) => {
        const firstKey = Object.keys(data)[0]

        const requestBody = {
          phone: formatTelephone(data[firstKey]),
          message,
          urlInstance,
        }

        setTimeout(async () => {
          try {
            await sendWhatsappText(requestBody)
          } catch (err) {
            console.log(err)
          }
        }, accumulator)

        accumulator += gen() * 1000
      })

      session.flash({ success: `Mensagens enfileiradas com sucesso!` })
      return response.redirect('back')
    } catch (err) {
      session.flash({ error: 'Problema ao enfileirar uma ou mais mensagens' })
      return response.redirect('back')
    }
  }
}

module.exports = SendWhatsappTextController
