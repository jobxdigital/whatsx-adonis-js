'use strict'

const Instance = use('App/Models/Instance')
const City = use('App/Models/City')

class InstanceController {
  async index({ view }) {
    return view.render('pages/register-instance')
  }

  async store({ request, response, session }) {
    try {
      const data = request.only(['name', 'api_id', 'api_token', 'city_id'])

      const city = await City.find(data.city_id)

      if (city) {
        await Instance.create(data)
      }

      session.flash({ success: 'Instância cadastrada com sucesso!' })
      return response.redirect('back')
    } catch (err) {
      session.flash({ error: 'Erro inesperado' })
      return response.redirect('back')
    }
  }
}

module.exports = InstanceController
