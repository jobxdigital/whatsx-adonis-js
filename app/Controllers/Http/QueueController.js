'use strict'

class QueueController {
  index({ view }) {
    return view.render('pages/queue')
  }
}

module.exports = QueueController
