'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')
const User = use('App/Models/User')
const { validate } = use('Validator')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with logins
 */
class LoginController {
  async create({ response, view, auth, session }) {
    try {
      await auth.check()
      return response.redirect('/home')
    } catch (error) {
      return view.render('pages/login')
    }
  }

  async store({ request, response, session, auth }) {
    try {
      const rules = {
        email: 'required|email',
        password: 'required',
      }

      const validation = await validate(request.all(), rules, {
        'email.required': 'Por favor, digite um endereço de email.',
        'email.email': 'Por favor, digite um endereço de email válido.',
        'password.required': 'Senha é um campo obrigatório',
      })

      if (validation.fails()) {
        session.withErrors(validation.messages()).flashExcept(['password'])

        return response.redirect('back')
      }

      const { email, password } = request.all()

      const data = await User.findBy('email', email)

      if (!data) {
        session.flash({ trySession: 'Email e/ou senha inválidos' })
        return response.redirect('back')
      }

      const equal = await Hash.verify(password, data.password)

      if (!equal) {
        session.flash({ trySession: 'Email e/ou senha inválidos' })
        return response.redirect('back')
      }

      await auth.attempt(email, password)

      return response.redirect('/home')
    } catch (err) {
      session.flash({
        trySession: 'Algum problema inesperado aconteceu. Desculpa.',
      })
      return response.redirect('back')
    }
  }

  async destroy({ request, response, auth, session }) {
    try {
      await auth.logout()
      session.clear()

      return response.redirect('/')
    } catch (err) {
      session.flash({ error: 'Erro inesperado' })
      return response.redirect('back')
    }
  }
}

module.exports = LoginController
