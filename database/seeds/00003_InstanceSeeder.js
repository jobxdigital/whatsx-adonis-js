'use strict'

/*
|--------------------------------------------------------------------------
| InstanceSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const City = use('App/Models/City')
const Instance = use('App/Models/Instance')

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class InstanceSeeder {
  async run () {
    const city = await City.find(1)
    if(!city){
      throw new Error(city + ' is not a city')
    }
    await Instance.create({
      name: 'Portal',
      api_token: '23267A9978BFC9FCBD1ED66B',
      api_id: '386212111419610A44470242AC110002',
      city_id: city.id
    })
  }
}

module.exports = InstanceSeeder
