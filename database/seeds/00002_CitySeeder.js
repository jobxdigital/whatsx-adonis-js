'use strict'

/*
|--------------------------------------------------------------------------
| CitySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const City = use('App/Models/City')

class CitySeeder {
  async run () {
    await City.create({ name: 'Mangabeiras' })
  }
}

module.exports = CitySeeder
