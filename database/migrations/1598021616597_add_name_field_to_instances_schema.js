'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddNameFieldToInstancesSchema extends Schema {
  up () {
    this.table('instances', (table) => {
      // alter table
      table.string('name').notNullable()
    })
  }

  down () {
    this.table('instances', (table) => {
      table.dropColumn('name')
    })
  }
}

module.exports = AddNameFieldToInstancesSchema
