'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ListsSchema extends Schema {
  up () {
    this.create('lists', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.string('csv_path').notNullable()
      table.string('tema').notNullable()

      table.integer('instance_id').notNullable().unsigned()
      table.foreign('instance_id').references('id').inTable('instances')
      table.timestamps()
    })
  }

  down () {
    this.drop('lists')
  }
}

module.exports = ListsSchema
