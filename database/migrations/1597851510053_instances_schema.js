'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InstancesSchema extends Schema {
  up () {
    this.create('instances', (table) => {
      table.increments()
      table.string('api_token').notNullable()
      table.string('api_id').notNullable()

      table.integer('city_id').notNullable().unsigned()
      table.foreign('city_id').references('id').inTable('cities')

      table.timestamps()
    })
  }

  down () {
    this.drop('instances')
  }
}

module.exports = InstancesSchema
