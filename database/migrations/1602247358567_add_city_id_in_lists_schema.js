'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddCityIdInListsSchema extends Schema {
  up () {
    this.table('lists', (table) => {
      table.integer('city_id').nullable().unsigned()
      table.foreign('city_id').references('id').inTable('cities')
    })
  }

  down () {
    this.table('lists', (table) => {
      table.dropForeign('city_id')
      table.dropColumn('city_id')
    })
  }
}

module.exports = AddCityIdInListsSchema
