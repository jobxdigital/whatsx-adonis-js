'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UpdateListFieldTemaToThemeSchema extends Schema {
  up () {
    this.table('lists', (table) => {
      table.renameColumn('tema', 'theme')
    })
  }

  down () {
    this.table('lists', (table) => {
      table.renameColumn('theme', 'tema')
    })
  }
}

module.exports = UpdateListFieldTemaToThemeSchema
