'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RemoveInstanceIdFromListsSchema extends Schema {
  up () {
    this.table('lists', (table) => {
      // alter table
      table.dropForeign('instance_id', 'lists_instance_id_foreign')
      table.dropColumn('instance_id')
    })
  }

  down () {
    this.table('lists', (table) => {
      table.integer('instance_id').notNullable().unsigned()
      table.foreign('instance_id').references('id').inTable('instances')
    })
  }
}

module.exports = RemoveInstanceIdFromListsSchema
