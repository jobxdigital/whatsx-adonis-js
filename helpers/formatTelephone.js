module.exports = function formatTelephone(tel = ''){
  let formatted = tel.replace(/\+|\(|\)|\ |\-/g, '').trim()

  if(formatted[0] !== '5' && formatted[1] !== '5'){
    formatted = '55' + formatted
  }

  return formatted
}
