const fs = require('fs')
const csv = require('csv-parser')
const combined = require('combined-stream')

module.exports = function (csv_paths, onData = () => {}, onEnd = () => {}) {
  if (typeof onData !== 'function') {
    throw new Error(onData + ' is not a function')
  }

  if (typeof onEnd !== 'function') {
    throw new Error(onEnd + ' is not a function')
  }

  const streams = csv_paths.map((csv_path) => {
    return fs.createReadStream(csv_path)
  })

  const combinedStreams = combined.create()

  streams.forEach((stream) => {
    combinedStreams.append(stream)
  })

  combinedStreams.pipe(csv()).on('data', onData).on('end', onEnd)
}
