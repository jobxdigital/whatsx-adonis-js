const axios = require('axios').default

module.exports = async ({ image, phone, urlInstance }) => {
  return await axios.post(
    `${urlInstance}/send-image`,
    {
      image,
      phone,
    },
    {
      headers: {
        'content-type': 'application/json'
      }
    }
  )
}
