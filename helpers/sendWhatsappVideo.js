const axios = require('axios').default

module.exports = async ({ video, phone, urlInstance }) => {
  return await axios.post(
    `${urlInstance}/send-video`,
    {
      video,
      phone
    },
    {
      headers: {
        'content-type': 'application/json'
      }
    }
  )
}
