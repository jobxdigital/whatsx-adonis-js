const fs = require('fs')
const csv = require('csv-parser')

module.exports = function (csv_path, onData = () => {}, onEnd = () => {}){
  if(typeof onData !== 'function'){
    throw new Error(onData + ' is not a function')
  }

  if(typeof onEnd !== 'function'){
    throw new Error(onEnd + ' is not a function')
  }

  const readStream = fs.createReadStream(csv_path)

  readStream
    .pipe(csv())
    .on('data', onData)
    .on('end', onEnd)

  return readStream;
}
