module.exports = function({ max, min, diff }){
  const history = [max]

  return function(){
    const last = history[history.length - 1]

    let limitUp = last + diff
    if(limitUp > max) limitUp = max

    let limitDown = last - diff
    if(limitDown < min) limitDown = min

    let nextDelay = Math.floor(Math.random() * (max - min + 1)) + min

    while(nextDelay >= limitDown && nextDelay <= limitUp){
      nextDelay = Math.floor(Math.random() * (max - min + 1)) + min
    }

    history.push(nextDelay)

    return nextDelay
  }
}
