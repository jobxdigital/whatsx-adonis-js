const axios = require('axios').default

module.exports = async ({ message, phone, urlInstance }) => {
  return await axios.post(
    `${urlInstance}/send-text`,
    {
      message,
      phone
    },
    {
      headers: {
        'content-type': 'application/json'
      }
    }
  )
}
