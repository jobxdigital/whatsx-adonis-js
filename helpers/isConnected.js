const axios = require('axios').default

module.exports = async function (urlInstance) {
  let connected = false

  const { data } = await axios.get(`${urlInstance}/status`)

  if(data.hasOwnProperty('connected') && data.connected){
    connected = true
  }

  return connected
}
