'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', 'LoginController.create')
Route.post('/login', 'LoginController.store')

Route.group(() => {
  Route.get('/logout', 'LoginController.destroy')
  Route.get('/home', 'PlatformController.index').middleware(['list'])

  Route.get('/register-city', 'CityController.index')
  Route.post('/register-city', 'CityController.store').validator('StoreCity')

  Route.get('/register-instance', 'InstanceController.index')
  Route.post('/register-instance', 'InstanceController.store').validator(
    'StoreInstance'
  )

  Route.get('/register-list', 'ListController.index')
  Route.post('/register-list', 'ListController.store').validator('StoreList')

  Route.get('/session/city/:city', 'SessionController.changeCity')
  Route.get('/session/instance/:instance', 'SessionController.changeInstance')

  Route.post('/whatsapp/text', 'SendWhatsappTextController.store')
  // .validator('SendWhatsappText')

  Route.get('/whatsapp/image', 'SendWhatsappImageController.index').middleware([
    'list',
  ])
  Route.post('/whatsapp/image', 'SendWhatsappImageController.store').validator(
    'SendWhatsappImage'
  )

  // VIDEO
  Route.get('/whatsapp/video', 'SendWhatsappVideoController.index').middleware([
    'list',
  ])
  Route.post('/whatsapp/video', 'SendWhatsappVideoController.store').validator(
    'SendWhatsappVideo'
  )

  // UPLOAD SINGLE VIDEO
  Route.get('/upload/video', 'OnlyVideoUploadController.index')
  Route.post('/upload/video', 'OnlyVideoUploadController.store')

  // QUEUE
  Route.get('/queue', 'QueueController.index')

  // TEXT AND IMAGE
  Route.get(
    '/whatsapp/text-image',
    'SendWhatsappTextImageController.index'
  ).middleware(['list'])
  Route.post(
    '/whatsapp/text-image',
    'SendWhatsappTextImageController.store'
  ).validator('SendWhatsappTextImage')

  // TEXT AND VIDEO UPLOAD
  Route.get('/upload/text-video', 'VideoTextUploadController.index')
  Route.post('/upload/text-video', 'VideoTextUploadController.store')

  Route.get(
    '/whatsapp/text-video',
    'SendWhatsappTextVideoController.index'
  ).middleware(['list'])
  Route.post(
    '/whatsapp/text-video',
    'SendWhatsappTextVideoController.store'
  ).validator('SendWhatsappVideoText')

}).middleware([
  'auth',
  'currentCity',
  'cities',
  'currentInstance',
  'instancesList',
  'linkApi',
])
